
# заготовка для домашней работы
# прочитайте про glob.glob
# https://docs.python.org/3/library/glob.html

# Задание
# мне нужно отыскать файл среди десятков других
# я знаю некоторые части этого файла (на память или из другого источника)
# я ищу только среди .sql файлов
# 1. программа ожидает строку, которую будет искать (input())
# после того, как строка введена, программа ищет её во всех файлах
# выводит список найденных файлов построчно
# выводит количество найденных файлов
# 2. снова ожидает ввод
# поиск происходит только среди найденных на этапе 1
# 3. снова ожидает ввод
# ...
# Выход из программы программировать не нужно.
# Достаточно принудительно остановить, для этого можете нажать Ctrl + C

# Пример на настоящих данных

# python3 find_procedure.py
# Введите строку: INSERT
# ... большой список файлов ...
# Всего: 301
# Введите строку: APPLICATION_SETUP
# ... большой список файлов ...
# Всего: 26
# Введите строку: A400M
# ... большой список файлов ...
# Всего: 17
# Введите строку: 0.0
# Migrations/000_PSE_Application_setup.sql
# Migrations/100_1-32_PSE_Application_setup.sql
# Всего: 2
# Введите строку: 2.0
# Migrations/000_PSE_Application_setup.sql
# Всего: 1

# не забываем организовывать собственный код в функции
# на зачёт с отличием, использовать папку 'Advanced Migrations'

import glob
import os.path
import re
import chardet


BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def read_dir_for_files(dir_name, pattern):
	files = glob.glob(os.path.join(BASE_DIR, os.path.join(dir_name, pattern)))
	return files

def search_in_file(files, pattern):
	sorted_files =[]
	for file in files:
		with open(file, 'rb', encoding=None) as file_data:
			raw_data = file_data.read()
			result = chardet.detect(raw_data)
			data = raw_data.decode(result['encoding'])
			result = re.search(input_string, data)
		if result:
			sorted_files.append(file)
	return sorted_files

def print_file_list(file_list):
	for file in files:
		print(file)
	print('Всего файлов: {}'.format(len(files)))

folder = input('Введиде имя папки для поиска файлов *.sql: ')
files = read_dir_for_files(folder, '*.sql')
while True:
		input_string = input('Введите строку для поиска: ')
		file_list = search_in_file(files, input_string)
		print_file_list(file_list)
		files = file_list
